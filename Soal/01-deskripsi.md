## Apa itu DevOps.?

Menurut saya devOps adalah suatu pekerjaan yang meng-otomasi-kan proses antara software development team dan development team agar mereka dapat melakukan proses build, test dan release perangkat lunak lebih cepat dan lebih efisien.

## Seberapakah pentingkah DevOps.?

Karena saya belum pernah bersentuhan langsung dengan yang namanya devops jadi saya belum tahu seberapa pentingnya devops itu, tapi menurut artikel yang saya baca pekerjaan DevOps itu sangat penting karena dia bisa memperpendek siklus pengembangan sistem sambil memberikan fitur, perbaikan, dan pembaruan yang sejalan dengan tujuan bisnis.
