## Menambahkan text selain menggunakan Vim atau nano

**Langkah-langkah menyisipkan text**

1. Gunakan echo untuk menyisipkan text.
2. echo "Kaliman yang ini dimasukkan" >> namafile

**Gambar**
![SS Pertama](../gambar/SoalNomor8/SS1.png)
![SS Kedua](../gambar/SoalNomor8/SS2.png)
![SS Ketiga](../gambar/SoalNomor8/SS3.png)
