## Deploy Project Hi dev ke heroku

**Langkah-Langkahnya**

1. Yang pertama clone terlebih dahulu
   `git clone namaurl`
2. Setelah itu masuk kedalam folder todos, lalu npm install
   `cd todos > npm install`
3. Kalo heroku sudah terinstall, tinggal login saja dengan cara.
   `Heroku Login`
4. Setelah itu create folder di heroku melalui cli
   `create heroku namafolder`
5. Setelah itu masuk ke web heroku , lalu pilih folder yang telah kita buat sebelumnya di command bash cli
6. Pilih tab resources, lalu tambahkan add-ons database, ditugas ini saya menggunakan "Heroku Postgress"
7. Setalah add-ons heroku postgress telah berhasil terbuat, arahkan cursor ke menu heroku-postgress
8. Klik data setting, lalu pilih database credential
9. disana sudah terdapat data seperti, username, host password dll
10. replace data config.json yang ada pada file todos sesuai dengan data yang ada pada heroku
11. setelah itu ikuti push data yang telah kita buat sebelumnya ke heroku
    ` Git Add .`
    `Git status`
    `git commit -m "judul"`
    `git push heroku master`
12. tunggu semua proses selesai
13. https://tugasdev.herokuapp.com/

**_Disclaimer : saya tidak sempat melakukan screen shot_**
